RM=rm -f
SED=sed
CC=cc65
AS=ca65
CL=cl65
SP=sp65
AR=ar65

ifeq ($(CC65_HOME),)
	CC65_HOME=/usr/local/lib/cc65
endif
ifeq ($(CC65_INC),)
	CC65_INC=$(CC65_HOME)/include
endif
ifeq ($(CC65_ASMINC),)
	CC65_ASMINC=$(CC65_HOME)/asminc
endif

all:
	$(MAKE) buildfont
	$(CC) -t lynx haiku.c
	$(AS) -t lynx haiku.s
	$(CC) -t lynx main.c
	$(AS) -t lynx main.s
	$(CL) -t lynx -o main.lnx main.o haiku.o lynx.lib

buildfont:
	$(MAKE) addchar CHAR=Hexclam IDENT=fontexclam BASELINE=6
	$(MAKE) addchar CHAR=Hquote IDENT=fontquote BASELINE=6
	$(MAKE) addchar CHAR=Hhash IDENT=fonthash BASELINE=5
	$(MAKE) addchar CHAR=Hdollar IDENT=fontdollar BASELINE=7
	$(MAKE) addchar CHAR=Hpercent IDENT=fontpercent BASELINE=7
	$(MAKE) addchar CHAR=Hampersand IDENT=fontampersand BASELINE=6
	$(MAKE) addchar CHAR=Hsinglequote IDENT=fontsinglequote BASELINE=6
	$(MAKE) addchar CHAR=Hleftparen IDENT=fontleftparen BASELINE=7
	$(MAKE) addchar CHAR=Hrightparen IDENT=fontrightparen BASELINE=7
	$(MAKE) addchar CHAR=Hstar IDENT=fontstar BASELINE=5
	$(MAKE) addchar CHAR=Hplus IDENT=fontplus BASELINE=5
	$(MAKE) addchar CHAR=Hcomma IDENT=fontcomma BASELINE=1
	$(MAKE) addchar CHAR=Hminus IDENT=fontminus BASELINE=3
	$(MAKE) addchar CHAR=Hperiod IDENT=fontperiod BASELINE=1
	$(MAKE) addchar CHAR=Hslash IDENT=fontslash BASELINE=7
	$(MAKE) addchar CHAR=H0 IDENT=font0 BASELINE=6
	$(MAKE) addchar CHAR=H1 IDENT=font1 BASELINE=6
	$(MAKE) addchar CHAR=H2 IDENT=font2 BASELINE=6
	$(MAKE) addchar CHAR=H3 IDENT=font3 BASELINE=6
	$(MAKE) addchar CHAR=H4 IDENT=font4 BASELINE=6
	$(MAKE) addchar CHAR=H5 IDENT=font5 BASELINE=6
	$(MAKE) addchar CHAR=H6 IDENT=font6 BASELINE=6
	$(MAKE) addchar CHAR=H7 IDENT=font7 BASELINE=6
	$(MAKE) addchar CHAR=H8 IDENT=font8 BASELINE=6
	$(MAKE) addchar CHAR=H9 IDENT=font9 BASELINE=6
	$(MAKE) addchar CHAR=Hcolon IDENT=fontcolon BASELINE=3
	$(MAKE) addchar CHAR=Hsemicolon IDENT=fontsemicolon BASELINE=3
	$(MAKE) addchar CHAR=Hlessthan IDENT=fontlessthan BASELINE=6
	$(MAKE) addchar CHAR=Hequal IDENT=fontequal BASELINE=4
	$(MAKE) addchar CHAR=Hgreaterthan IDENT=fontgreaterthan BASELINE=6
	$(MAKE) addchar CHAR=Hquestion IDENT=fontquestion BASELINE=7
	$(MAKE) addchar CHAR=Hat IDENT=fontat BASELINE=3
	$(MAKE) addchar CHAR=HA IDENT=fontA BASELINE=6
	$(MAKE) addchar CHAR=HB IDENT=fontB BASELINE=6
	$(MAKE) addchar CHAR=HC IDENT=fontC BASELINE=7
	$(MAKE) addchar CHAR=HD IDENT=fontD BASELINE=6
	$(MAKE) addchar CHAR=HE IDENT=fontE BASELINE=7
	$(MAKE) addchar CHAR=HF IDENT=fontF BASELINE=7
	$(MAKE) addchar CHAR=HG IDENT=fontG BASELINE=7
	$(MAKE) addchar CHAR=HH IDENT=fontH BASELINE=6
	$(MAKE) addchar CHAR=HI IDENT=fontI BASELINE=6
	$(MAKE) addchar CHAR=HJ IDENT=fontJ BASELINE=6
	$(MAKE) addchar CHAR=HK IDENT=fontK BASELINE=6
	$(MAKE) addchar CHAR=HL IDENT=fontL BASELINE=6
	$(MAKE) addchar CHAR=HM IDENT=fontM BASELINE=6
	$(MAKE) addchar CHAR=HN IDENT=fontN BASELINE=6
	$(MAKE) addchar CHAR=HO IDENT=fontO BASELINE=6
	$(MAKE) addchar CHAR=HP IDENT=fontP BASELINE=6
	$(MAKE) addchar CHAR=HQ IDENT=fontQ BASELINE=6
	$(MAKE) addchar CHAR=HR IDENT=fontR BASELINE=6
	$(MAKE) addchar CHAR=HS IDENT=fontS BASELINE=7
	$(MAKE) addchar CHAR=HT IDENT=fontT BASELINE=7
	$(MAKE) addchar CHAR=HU IDENT=fontU BASELINE=6
	$(MAKE) addchar CHAR=HV IDENT=fontV BASELINE=6
	$(MAKE) addchar CHAR=HW IDENT=fontW BASELINE=6
	$(MAKE) addchar CHAR=HX IDENT=fontX BASELINE=6
	$(MAKE) addchar CHAR=HY IDENT=fontY BASELINE=6
	$(MAKE) addchar CHAR=HZ IDENT=fontZ BASELINE=7
	$(MAKE) addchar CHAR=Hunderscore IDENT=fontunderscore BASELINE=0
	$(MAKE) addchar CHAR=Ha IDENT=fonta BASELINE=6
	$(MAKE) addchar CHAR=Hb IDENT=fontb BASELINE=6
	$(MAKE) addchar CHAR=Hc IDENT=fontc BASELINE=5
	$(MAKE) addchar CHAR=Hd IDENT=fontd BASELINE=6
	$(MAKE) addchar CHAR=He IDENT=fonte BASELINE=4
	$(MAKE) addchar CHAR=Hf IDENT=fontf BASELINE=5
	$(MAKE) addchar CHAR=Hg IDENT=fontg BASELINE=4
	$(MAKE) addchar CHAR=Hh IDENT=fonth BASELINE=6
	$(MAKE) addchar CHAR=Hi IDENT=fonti BASELINE=6
	$(MAKE) addchar CHAR=Hj IDENT=fontj BASELINE=6
	$(MAKE) addchar CHAR=Hk IDENT=fontk BASELINE=6
	$(MAKE) addchar CHAR=Hl IDENT=fontl BASELINE=6
	$(MAKE) addchar CHAR=Hm IDENT=fontm BASELINE=4
	$(MAKE) addchar CHAR=Hn IDENT=fontn BASELINE=4
	$(MAKE) addchar CHAR=Ho IDENT=fonto BASELINE=4
	$(MAKE) addchar CHAR=Hp IDENT=fontp BASELINE=4
	$(MAKE) addchar CHAR=Hq IDENT=fontq BASELINE=4
	$(MAKE) addchar CHAR=Hr IDENT=fontr BASELINE=4
	$(MAKE) addchar CHAR=Hs IDENT=fonts BASELINE=5
	$(MAKE) addchar CHAR=Ht IDENT=fontt BASELINE=6
	$(MAKE) addchar CHAR=Hu IDENT=fontu BASELINE=4
	$(MAKE) addchar CHAR=Hv IDENT=fontv BASELINE=4
	$(MAKE) addchar CHAR=Hw IDENT=fontw BASELINE=4
	$(MAKE) addchar CHAR=Hx IDENT=fontx BASELINE=4
	$(MAKE) addchar CHAR=Hy IDENT=fonty BASELINE=4
	$(MAKE) addchar CHAR=Hz IDENT=fontz BASELINE=5

addchar:
	$(SP) -r $(CHAR).pcx -c lynx-sprite,mode=literal,ay=$(BASELINE) \
		-w $(CHAR).c,ident=$(IDENT),bytesperline=8
	$(SED) -i 's/const/static const/' $(CHAR).c

clean:
	$(RM) *.o
	$(RM) H*.c
	$(RM) H*.s
	$(RM) main.s
	$(RM) main.lnx

